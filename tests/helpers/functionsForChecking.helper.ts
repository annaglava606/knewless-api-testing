import { expect } from 'chai';

var chai = require('chai');
chai.use(require('chai-json-schema'));

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseBodyStatus(response, status: string) {
    expect(response.body.status, `Status should be ${status}`).to.be.equal(status);
}

export function checkResponseBodyMessage(response, message: string) {
    expect(response.body.message, `Message should be ${message}`).to.be.equal(message);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}

export function checkBodyContainsProperty(response, property: string) {
    expect(response.body,`Body should contain ${property}`).to.have.property(property);
}

export function checkBodyHasSchema(response, schema: object) {
    expect(response.body,'Body should have right schema').to.be.jsonSchema(schema);
}

export function checkPropertyIsNull(property) {
    expect(property).to.be.null;
}