import { expect } from 'chai';
import { 
    checkResponseTime, 
    checkStatusCode, 
    checkBodyContainsProperty, 
    checkBodyHasSchema, 
    checkResponseBodyMessage, 
    checkPropertyIsNull 
} from '../../helpers/functionsForChecking.helper';
import { ArticlesController } from '../lib/controllers/articles.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import { AuthorController } from '../lib/controllers/author.controller';
import { UserController } from '../lib/controllers/user.controller';
const auth = new AuthController();
const author = new AuthorController();
const user = new UserController();
const article = new ArticlesController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

const email = global.appConfig.users.author.email;
const password = global.appConfig.users.author.password;

describe('Author chain', () => {
    let accessToken;
    let authorId;
    let userId;
    let articleId;

    before('should get acces token and userId', async () => {
        // runs once before the first test in this block
        let response = await auth.authenticateUser(email, password);
    
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_userLogin);
        checkBodyContainsProperty(response,'accessToken');
        checkBodyContainsProperty(response,'refreshToken');

        accessToken = response.body.accessToken;

        response = await author.getSettings(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_authorSettings);
        checkBodyContainsProperty(response,'userId');
        checkBodyContainsProperty(response, 'id');

        checkPropertyIsNull(response.body.avatar);
        expect(response.body.firstName).to.equal('Anita');
        expect(response.body.lastName).to.equal('Petrova');
        checkPropertyIsNull(response.body.job);
        expect(response.body.location).to.equal('Afghanistan');
        checkPropertyIsNull(response.body.company);
        checkPropertyIsNull(response.body.website);
        expect(response.body.twitter).to.equal('https://twitter.com/');
        checkPropertyIsNull(response.body.biography);

        authorId = response.body.id;
        userId = response.body.userId;
    });

    it('Author set settings', async () => {
        const authorSettings = {
            id: authorId,
            userId: userId,
            firstName: 'Anita',
            lastName: 'Petrova',
            location: 'Afghanistan',
            twitter: 'https://twitter.com/'
        };
        const response = await author.setSettings(accessToken, authorSettings);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkResponseBodyMessage(response, 'Success. Your profile has been updated.');
    });

    it('Current user info', async () => {
        const response = await user.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_currentUserInfo);

        expect(response.body.id).to.equal(userId);
        expect(response.body.email).to.equal(email);
        checkPropertyIsNull(response.body.nickname);
        expect(response.body.role.name).to.equal('AUTHOR');
        checkPropertyIsNull(response.body.avatar);
        expect(response.body.emailVerified).to.be.true;
    });

    it('Author overview', async () => {
        const response = await author.getAuthorOverview(accessToken, authorId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_authorOverview);

        expect(response.body.id).to.equal(authorId);
        expect(response.body.userId).to.equal(userId);
        expect(response.body.firstName).to.equal('Anita');
        expect(response.body.lastName).to.equal('Petrova');
        checkPropertyIsNull(response.body.avatar);
        checkPropertyIsNull(response.body.biography);
        expect(response.body.schoolName).to.be.empty;
        expect(response.body.schoolId).to.be.empty;
        expect(response.body.numberOfSubscribers).to.equal(0);
        expect(response.body.courses).to.be.empty;
        expect(response.body).to.have.property('articles');

    });

    it('Articles creation', async () => {
        const articleData = {
            name: 'articletitle',
            image: "https://knewless.tk/assets/images/45ca34d4-cfd2-4811-ad82-b2312adefc43.PNG" ,
            text: 'articletext'
        };
        const response = await article.saveArticle(accessToken, articleData);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_articlesCreation);
        checkBodyContainsProperty(response,'id');

        expect(response.body.name).to.equal(articleData.name);
        expect(response.body.text).to.equal(articleData.text);
        expect(response.body.image).to.equal(articleData.image);
        expect(response.body.authorId).to.equal(authorId);
        expect(response.body.authorName).to.equal('Anita Petrova');

        articleId = response.body.id;
    });

    it('Article author', async () => {
        const response = await article.getArticles(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_articleAuthor);

        expect(response.body.length).to.be.above(0);
        expect(response.body[0].authorId).to.equal(authorId);
    });

    it('Article id', async () => {
        const response = await article.getArticle(accessToken, articleId);
       
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_articleId);

        checkPropertyIsNull(response.body.id);
        expect(response.body.name).to.equal('articletitle');
        expect(response.body.text).to.equal('articletext');
        expect(response.body.image).to.equal('https://knewless.tk/assets/images/45ca34d4-cfd2-4811-ad82-b2312adefc43.PNG');
        expect(response.body.author.id).to.equal(authorId);
        expect(response.body.author.userId).to.equal(userId);
        expect(response.body.author.name).to.equal('Anita Petrova');
        checkPropertyIsNull(response.body.author.avatar);
        checkPropertyIsNull(response.body.author.biography);
        expect(response.body.author.articles.length).to.be.above(0);
        expect(response.body.favourite).to.be.false;
    });    

    it('Article comment', async () => {
        const response = await article.saveComment(accessToken, articleId, authorId, "Comment text");

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_articleComment);
        checkBodyContainsProperty(response,'id');

        expect(response.body.text).to.equal('Comment text');
        expect(response.body.user.id).to.equal(userId);
        checkPropertyIsNull(response.body.user.username);
        expect(response.body.user.email).to.equal(email);
        expect(response.body.user.role).to.equal('AUTHOR');
        expect(response.body.articleId).to.equal(articleId);
        expect(response.body).to.have.property('sourceId');
    }); 

    it('Article comments', async () => {
        const response = await article.getArticleComments(accessToken, articleId);
  
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_articleComments);

        expect(response.body.length).to.be.above(0);
        expect(response.body[0]).to.have.property('id');
        expect(response.body[0].text).to.equal('Comment text');
        expect(response.body[0].user.id).to.equal(userId);
        checkPropertyIsNull(response.body[0].user.username);
        expect(response.body[0].user.email).to.equal(email);
        expect(response.body[0].user.role).to.equal('AUTHOR');
        expect(response.body[0].articleId).to.equal(articleId);
        expect(response.body[0]).to.have.property('sourceId');
    }); 

    afterEach(function (){
    //runs after each test in this block
    console.log ('it was a test');
    });

})