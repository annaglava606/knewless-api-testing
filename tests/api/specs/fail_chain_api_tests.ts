import { expect } from 'chai';
import { 
    checkResponseTime, 
    checkStatusCode, 
    checkBodyContainsProperty, 
    checkBodyHasSchema, 
    checkResponseBodyMessage, 
    checkPropertyIsNull 
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
import { UserController } from '../lib/controllers/user.controller';
import { StudentController } from '../lib/controllers/student.controller';
const auth = new AuthController();
const user = new UserController();
const student = new StudentController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

const email = "wynona.izza@moondoo.org";
const password = "Dadadadada12";

describe('Fail tests chain', () => {
    let accessToken;
    let studentId;
    let userId;


    it('should get acces token and userId', async () => {
        let response = await auth.authenticateUser(email, password);
    
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_userLogin);

        checkBodyContainsProperty(response,'accessToken');

        expect(response.body).to.have.property('refreshToken');

        accessToken = response.body.accessToken;

        response = await user.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_currentUserInfo);
        checkBodyContainsProperty(response,'userId');

        expect(response.body).to.have.property('id');
        expect(response.body.email).to.equal(email);
        expect(response.body.nickname).to.equal('marinahaker2000');
        expect(response.body.role.name).to.equal('USER');
        checkPropertyIsNull(response.body.avatar);
        expect(response.body.emailVerified).to.be.true;

        userId = response.body.id
    });


    it('Get student settings', async () => {
        const response = await student.getSettings(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        expect(response.body.userId).to.equal(userId);

        checkBodyContainsProperty(response,'id');
    
        checkPropertyIsNull(response.body.avatar);
        expect(response.body.firstName).to.equal("Anita");
        expect(response.body.lastName).to.equal("Petrova");
        expect(response.body.job).to.be.empty;
        expect(response.body.location).to.equal('Afghanistan');
        expect(response.body.company).to.be.empty;
        expect(response.body.company).to.be.empty;
        expect(response.body.biography).to.be.empty;
        expect(response.body.direction).to.equal('Developer');
        expect(response.body.experience).to.equal(0);
        expect(response.body.level).to.equal("Beginner");
        expect(response.body.industry).to.equal("Government");
        expect(response.body.role).to.equal("DevOps");
        expect(response.body.employment).to.equal("Employee");
        expect(response.body.education).to.equal("Middle School");
        expect(response.body.year).to.equal(2000);
        expect(response.body.tags).to.be.empty;

        studentId = response.body.userId;
    });

    it('Set student settings', async () => {

        let settings = {
            id: '96a624ac-02c2-4da1-93e5-2f3e7ab5f6b7_',
            userId: '477ac836-a9ea-4e2c-927d-612218fa0e80_',
            avatar: null,
            firstName: 'Anita',
            lastName: 'Petrova',
            job: '',
            location: 'Afghanistan',
            company: '',
            website: '',
            biography: '',
            direction: 'Developer',
            experience: 0,
            level: 'Beginner',
            industry: 'Government',
            role: 'DevOps',
            employment: 'Employee',
            education: 'Middle School',
            year: 1950,
            tags: []
        }

        const response = await student.setSettings(accessToken, settings);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkResponseBodyMessage(response, "Success. Your profile has been updated.");
    });

    it('Set student goal', async () => {
        let goal = {
            goalId: "f1b1f41b-78bd-44d1-8be9-cad85d9ee750_"
        }

        const response = await student.setGoal(accessToken, goal);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        expect(response.body).to.be.empty;
    });

    it('Get student goal', async () => {
        const response = await student.getGoal(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyHasSchema(response, schemas.schema_getStudentGoal);
        checkBodyContainsProperty(response,'goalId');
    
        expect(response.body.goalName).to.equal("60 minutes a week");
        expect(response.body.percentsDone).to.equal(0);
        expect(response.body.secondsDone).to.equal(0);
        expect(response.body.secondsLeft).to.equal(1800);
        expect(response.body.secondsNeededOverall).to.equal(1800);
        expect(response.body.congratulationShown).to.be.false;
        expect(response.body.done).to.be.false;
    });

    afterEach(function (){
        //runs after each test in this block
        console.log ('it was a test');
    });
    
})