import {
    checkResponseTime,
    checkStatusCode,
    checkResponseBodyStatus,
    checkResponseBodyMessage,
    checkBodyContainsProperty,
    checkBodyHasSchema,

} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');

describe('Use test data', () => {
    let invalidCredentialsDataSet = [
        { email: '2jem8yit5z@email.edu.pl', password: '      ' },
        { email: '2jem8yit5z@email.edu.pl', password: '45dadaad' },
        { email: '2jem8yit5z@email.edu.pl', password: '2jem8yit5z@email.edu.pl' },
        { email: '2jem8yit5z@email.edu.pl', password: 'admin' },
        { email: '2jem8yit5z@email.edu.pl', password: 'anna94anna' },
        { email: '2jem8yit5z@email.edu.pl', password: 'testData2022' },
        { email: '2jem8yit5z@email.edu.pl', password: 'admin12345678' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await auth.authenticateUser(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, 'UNAUTHORIZED');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });

    let validCredentialsDataSet = [
        { email: '2jem8yit5z@email.edu.pl', password: 'dadada4545' },
        { email: 'wynona.izza@moondoo.org', password: 'Dadadadada12' },
        { email: 'puk84526@cdfaq.com', password: '123456789qwerty' },
        { email: 'jiyasa1549@5k2u.com', password: '8448asdf' },
    ];

    validCredentialsDataSet.forEach((credentials) => {
        it(`login using valid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await auth.authenticateUser(credentials.email, credentials.password);

            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);
            checkBodyHasSchema(response,schemas.schema_userLogin);
            checkBodyContainsProperty(response,'accessToken');
            checkBodyContainsProperty(response,'refreshToken');
        });
    });
});
