import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class ArticlesController {
    async getArticles(accessToken: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`article/author`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async saveArticle(accessToken: string, articleObj: object) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`article`)
            .bearerToken(accessToken)
            .body(articleObj)
            .send();
        return response;
    }

    async saveComment(accessToken: string, articleIdValue: string, idValue: string, textValue: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`article_comment`)
            .bearerToken(accessToken)
            .body({
                articleId: articleIdValue,
                text: textValue,
            })
            .send();
        return response;
    }

    async getArticle(accessToken: string, articleId: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`article/`+articleId)
            .bearerToken(accessToken)
            .send();
        return response;
    }


    async getArticleComments(accessToken: string, articleId: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`article_comment/of/` + articleId + '?size=200')
            .bearerToken(accessToken)
            .send();
        return response;
    }


}
