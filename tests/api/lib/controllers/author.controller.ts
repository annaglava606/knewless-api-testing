import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class AuthorController {
    async getAuthorInfo(accessToken: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`author/self-info`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getAuthorOverview(accessToken: string, userId: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`author/overview/`+ userId)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getSettings(accessToken: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`author`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async setSettings(accessToken: string, authorSettings: object) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`author`)
            .bearerToken(accessToken)
            .body(authorSettings)
            .send();
        return response;
    }
}
