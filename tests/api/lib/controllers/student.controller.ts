import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class StudentController {
    async getSettings(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`student`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async setSettings(accessToken: string, settings: object) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`student`)
            .bearerToken(accessToken)
            .body(settings)
            .send();
        return response;
    }

    async setGoal(accessToken: string, goal: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`student/goal`)
            .bearerToken(accessToken)
            .body(goal)
            .send();
        return response;
    }

    async getGoal(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`student/goal`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
